from tkinter import *
import datetime
import open_file

dane = open_file.open_file()


def windows():


    window = Tk()
    window.title("Lista Obecności")
    window.wm_minsize(width=460, height=460)

    def onReturn(event):
        print("process")
        frame_data_entry_value = frame_data_entry.get();
        print(frame_data_entry_value)

    set_data = StringVar()
    foo = datetime.datetime.now()
    eggs = (f'{foo.day}-{foo.month}-{foo.year}')
    set_data.set(eggs)

    frame_data = Label(window, text="Wprowadź datę:", font=(12))
    frame_data.grid(row=0, ipadx=15, ipady=15, column=0)
    frame_data_entry = Entry(window, bg="silver", font=(10), justify=CENTER, text=set_data)
    frame_data_entry.grid(row=0, ipadx=0, ipady=5, column=1)
    frame_data_entry.bind("<Return>", onReturn)



    frame_top = Label(window, text="Imię i Nazwisko", font=('Arial', 12))
    frame_top.grid(row=1, ipadx=15, ipady=10, column=0)

    i = 0
    name = 0

    for i in range(len(dane)):
        v = IntVar()
        name = Label(window, text=dane[i], font=('Arial', 12, 'italic'))
        name.grid(row=[i + 2], ipadx=15, ipady=5, column=0)

        check = Checkbutton(window, text="Obecny", variable=v, onvalue=1)
        check.grid(row=[i + 2], column=1)
        check1 = Checkbutton(window, text="Nieobecny", variable=v, onvalue=2)
        check1.grid(row=[i + 2], column=2)

    def sub_windows():
        sub_window = Tk()
        sub_window.title("Lista")
        sub_window.wm_minsize(width=460, height=460)
        sub_window.config(bg="red")

        sub_window.mainloop()


    def onclick_but(args):

        if args == 1:
            print("Zapisz")
        if args == 2:
            print("Pokaż")
            print(dane)
        if args == 3:
            # plik.close()
            window.quit()
            window.destroy()

    def save_file():
        czas_i_data = frame_data_entry.get()
        plik = open(f'{czas_i_data}.txt', 'w', encoding=('utf8'))

    button_save = Button(window, text="Zapisz", command=lambda: onclick_but(save_file()))
    button_save.grid(row=[i + 3], column=0, ipadx=20, pady=20)
    button_show = Button(window, text="Pokaż", command=lambda: onclick_but(sub_windows()))
    button_show.grid(row=[i + 3], column=1, ipadx=20)
    button_close = Button(window, text="Zamknij", command=lambda: onclick_but(3))
    button_close.grid(row=[i + 3], column=2, ipadx=20)

    window.mainloop()


if __name__ == '__main__':
    windows()