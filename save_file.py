from tkinter import *
import datetime

''' fragment tylko do testu: START 
    pobieranie pliku w def: open_file.py
    ustawienie daty w Windows.py
'''

foo = datetime.datetime.now()
eggs = (f'{foo.day}-{foo.month}-{foo.year}')



plik = open (f'{eggs}.txt', 'w', encoding = ('utf8'))

'''       KONIEC  testowego fragmentu        '''

def save_file():

    '''  ta funkcja robi dwie rzeczy - otwera nowe okno i drukuje listę oraz zapisuje plik. Będzie do rozbicia.
      część otwierająca okno pójdzie w osobnej funkcji do przycisku Pokaż a zapisująca plik do przycisku Zapisz.

      Funkcja zpisu do modyfikacji. Musi pobierać tekst z pola: Wrpowadź datę:

      '''

    window = Tk()
    window.title("Pokazywanie")
    text1 = Text(window, width=30, height=20, pady=10, padx=10)
    text1.config(state="normal")



    lista = ['bla', 'bla1', 'yyy'] # < --- to w zasadzie tytlko testowo. Tu ta sama lista co w Windows.py - dane [] - docelowo do zamiany w niższym for'ze
    obecnosc = [1, 0, 1] # <-- tu powinna być lista pobrana ze stanu checkbox'ów 1 - obecny, 0 - nieobecny, choć można zrobić tekstowo bądź boolanem

    # dane = {}


    for i in range(len(lista)):
        # buff = (lista[i]+', '+ obecnosc[i]) + '\n'
        print ((f'{lista[i]} : {obecnosc[i]}'), file=plik )
        if obecnosc[i] == 0:
            j = "Nieobecny"
        else:
            j = "Obecny"
        print(j)
        text1.insert(INSERT, (f'{lista[i]} : {j}\n'))

    text1.pack()
    # plik.write('\n'.join(lista) + ',  ' + (obecnosc))
    # print(dane)

    window.mainloop()

if __name__ == '__main__':
    save_file()

# plik.write('\n'. join(lista))
    plik.close()

